/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import com.mysql.jdbc.ResultSetMetaData;
import comunes.Constantes;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ILACYERO
 */
public class ConexionMSQL {

    public static final String SQLDRIVER = "sun.jdbc.odbc.JdbcOdbcDriver";
    public static final String MYSQLDRIVER = "com.mysql.jdbc.Driver";
    public static final String SQL = "jdbc:odbc:";
    public static final String MYSQL = "jdbc:mysql:///";//INDICA LA BASE DE DATOS LOCAL
    private String driver;
    private String gestor;
    private String bd;
    private String usuario;
    private String password;
    private Connection conexion;
    private boolean cambio;

    public ConexionMSQL() {
        driver = this.MYSQLDRIVER;
        gestor = "jdbc:mysql://localhost/";
        bd = "BDHelpCenter";
        usuario = "root";
        password = "";
        cambio = false;
    }

    public ConexionMSQL(String driver, String gestor, String bd, String usuario, String password) {
        this.driver = driver;
        this.gestor = gestor;
        this.bd = bd;
        this.usuario = usuario;
        this.password = password;
        cambio = false;
    }

    public String getGestor() {
        return gestor;
    }

    public void setGestor(String gestor) {
        this.gestor = gestor;
        cambio = true;
    }

    public String getBd() {
        return bd;
    }

    public void setBd(String bd) {
        this.bd = bd;
        cambio = true;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
        cambio = true;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        cambio = true;
    }

    public void abrirConexion() throws SQLException {
        try {
            //if(conexion==null || cambio){
            java.lang.Class.forName(driver);
            String strCnx = gestor + bd + "?useServerPrepStmts=true";
            System.out.println(strCnx);
            conexion = DriverManager.getConnection(strCnx, usuario, password);
            cambio = false;
            // }
            //conexion.
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.println("Error al cargar el Driver");
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Error al conectarce con la Base de Datos : " + bd);
        }
    }

    public void cerrarConexion() {
        try {
            if (conexion != null) {
                conexion.close();
            }
        } catch (SQLException ex) {
            System.out.println("Error al cerrar la conexion con : " + bd);
        }
    }
    //CONSULTAS OSCAR***********************************

    public int actualizar(String sql) throws SQLException, Exception {
        abrirConexion();
        System.out.println("sentencia: " + sql);
        try {
            java.sql.Statement sentencia = conexion.createStatement();
            return sentencia.executeUpdate(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new Exception("Error al ejecutar comando : " + sql);
        } finally {
            cerrarConexion();
        }
    }

    public int actualizar(String sql, Object[] params) throws SQLException, Exception {
        abrirConexion();
        System.out.println("sentencia: " + sql);
        try {
            java.sql.PreparedStatement sentencia = conexion.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                sentencia.setObject(i + 1, params[i]);
            }
            return sentencia.executeUpdate();
        } catch (SQLException ex) {
            //System.out.println("Error al ejecutar comando : " + sql);
            ex.printStackTrace();
            throw new Exception("Error al ejecutar comando : " + sql);
        } finally {
            cerrarConexion();
        }
    }

    public ArraySQL consulta(String sql) throws SQLException, Exception {
        abrirConexion();
        System.out.println("sentencia: " + sql);
        try {
            java.sql.Statement sentencia = conexion.createStatement();
            return toArray(sentencia.executeQuery(sql));
        } catch (SQLException ex) {
            throw ex;
        } finally {
            cerrarConexion();
        }
    }

    public ArraySQL consulta(String sql, Object[] params) throws SQLException, Exception {
        abrirConexion();
        System.out.println("sentencia: " + sql);
        try {
            java.sql.PreparedStatement sentencia = conexion.prepareStatement(sql);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    sentencia.setObject(i + 1, params[i]);
                }
            }
            //java.sql.Statement sentencia = conexion.createStatement();
            return toArray(sentencia.executeQuery());
        } catch (SQLException ex) {
            throw ex;
        } finally {
            //cerrarConexion();
        }
    }

    private ArraySQL toArray(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                ResultSetMetaData metadata = (ResultSetMetaData) resultSet.getMetaData();
                int nroCol = metadata.getColumnCount();
                int nroFil = 1;
                if (resultSet.last()) {
                    nroFil = resultSet.getRow() + 1;
                }
                Object[][] datos = new Object[nroFil][nroCol];
                int fil = 0;
                Object[] columns = datos[fil];
                for (int i = 0; i < nroCol; i++) {
                    columns[i] = (metadata.getColumnName(i + 1));
                }
                fil++;
                if (resultSet.first()) {
                    while (!resultSet.isAfterLast()) {
                        Object[] tupla = datos[fil];
                        for (int i = 0; i < nroCol; i++) {
                            tupla[i] = (resultSet.getObject(i + 1));
                        }
                        resultSet.next();
                        fil++;
                    }
                }
                return new ArraySQL(datos);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }
    //OTRAS CONSULTAS

    public boolean probarConexion() {
        try {
            abrirConexion();
            return true;
        } catch (SQLException ex) {
            return false;
        } finally {
            cerrarConexion();
        }
    }

    public boolean existe(String consulta) throws Exception {
        return (consulta(consulta)).getCantTuplas() >= 1;
    }
    /**
     * Obtiene el ID requerido, se debe indicar:
     * @param  tabla  Nombre de la tabla en la BD
     * @param id_tabla ID de la tabla indicada, la ID QUE SE CONSULTARA
     * @param tipoAccion Indica PRIMERO,ANTERIOR,SIGUIENTE,ULTIMO, basados en Constatnes
     * @param actual  ID Actual o en mediante el cual se sacara el anterior y siguiente, en PRIMERO y ULTIMO, no interesa su valor
     * @param condicion Indica una condicion a la busqueda de ID
     * @param params  en caso de existir parametros se encuentran en el vector
     * @see Constantes
     */
    public int getIDCondicional(String tabla, String id_tabla, byte tipoAccion, int actual, String condicion, Object[] params) throws SQLException, Exception {
        String sql = "";
        if (condicion!=null && condicion.length() > 0) {
            if (tipoAccion == 0 || tipoAccion == 3) {
                condicion = " where " + condicion;
            } else {
                condicion = " and " + condicion;
            }
        }else
            condicion = "";
        //0:primer , 1:anterior , 2:siguiente , 3:ultimo
        switch (tipoAccion) {
            case Constantes.NAV_PRIMERO: {
                sql = "select min(" + id_tabla + ") from " + tabla + condicion;
            }
            break;
            //select id_tabla from tabla where id_tabla<5 order by id_tabla desc
            case Constantes.NAV_ANTERIOR: {
                sql = " SELECT " + id_tabla + " from " + tabla + " where " + id_tabla + " < " + actual + condicion + " order by " + id_tabla + " desc";
            }
            break;
            case Constantes.NAV_SIGUIENTE: {
                sql = "select " + id_tabla + " from " + tabla + " where " + id_tabla + " > " + actual + condicion;
            }
            break;
            case Constantes.NAV_ULTIMO: {
                sql = "select max(" + id_tabla + ") from " + tabla + condicion;
            }
            break;
        }
        ArraySQL res = consulta(sql, params);
        if (res.getCantTuplas() > 0) {
            res.first();
            String obj = String.valueOf(res.getData(0));
            if (obj.length() > 0 && !obj.contains("null")) {
                return Integer.parseInt(obj);
            } else {
                return -1;
            }
        } else {
            return -2;
        }
    }
    //CONSULTAS OSCAR***********************************

    public void ejecutarGestionar(String sql) {
        System.out.println("sentencia: " + sql);
        try {
            java.sql.Statement sentencia = conexion.createStatement();
            sentencia.executeUpdate(sql);

        } catch (SQLException ex) {
            System.out.println("Error al ejecutar comando : " + sql);
        }
    }

    public Vector ejecutarConsulta(String sql) {
        try {
            java.sql.Statement sentencia = conexion.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql);

            return toVectorSimple(toVector(resultado));
        } catch (SQLException ex) {
            System.out.println("Error al ejecutar consulta : " + sql);
        }
        return null;
    }

    public Vector ejecutarConsulta2(String sql) {
        try {
            java.sql.Statement sentencia = conexion.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql);
            Vector v = toVector(resultado);

            // v = [[ci, nombre, telf],[77, jaque, 73148858]]
            // v = [77, jaque, 73148858]
            return (Vector) v.get(1);

        } catch (SQLException ex) {
            System.out.println("Error al ejecutar consulta : " + sql);
        }
        return null;
    }

    public Vector ejecutarConsulta3(String sql) {
        try {
            java.sql.Statement sentencia = conexion.createStatement();
            ResultSet resultado = sentencia.executeQuery(sql);
            Vector v = toVector(resultado);

            // v = [[ci, nombre, telf],[77, jaque, 73148858],[88, yoshi, 73148858],[99, mack, 73148858]]
            return v;

        } catch (SQLException ex) {
            System.out.println("Error al ejecutar consulta : " + sql);
        }
        return null;
    }

    private static Vector toVector(ResultSet resultSet) {   // devuelve así v = [[letra],[a],[b],[c],[d]]
        if (resultSet != null) {
            try {
                ResultSetMetaData metadata = (ResultSetMetaData) resultSet.getMetaData();
                int nroCol = metadata.getColumnCount();
                Vector tabla = new Vector();
                Vector tupla = new Vector(nroCol);



                for (int i = 0; i < nroCol; i++) {
                    tupla.addElement(metadata.getColumnName(i + 1));
                }
                tabla.addElement(tupla);
                while (resultSet.next()) {
                    tupla = new Vector(nroCol);
                    for (int i = 0; i < nroCol; i++) {
                        tupla.addElement(resultSet.getObject(i + 1));
                    }
                    tabla.addElement(tupla);
                }
                //return tabla;
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    public Vector toVectorSimple(Vector result) { // devuelve sin títulos v = [a,b,c,d] y no asi
        // v = [[letra],[a],[b],[c],[d]]
        Vector aux = new Vector();
        for (int i = 1; i < result.size(); i++) {
            Vector v = (Vector) result.get(i);
            aux.add(v.get(0));
        }
        return aux;
    }

    public static void main(String[] args) {
        ConexionMSQL cnx = new ConexionMSQL();
        //cnx.setBd("BDHelpCenter");
        //cnx.setGestor("localhost");
        //cnx.setUsuario("root");
        // cnx.setPassword("");
        try {
            // System.out.println(cnx.actualizar("INSERT INTO Empresa VALUES (11,'OSCAR','70912097')"));
            //System.out.println(cnx.actualizar("INSERT INTO Empresa VALUES (?,?,?)",new Object[]{20,"OSCAR 2","PROBANDO"}));
            //ResultSet res=cnx.consulta("SELECT * FROM EMPRESA WHERE IDEmpresa = ?",new Object[]{10});
            /*ResultSet res=cnx.consulta("SELECT * FROM EMPRESA WHERE IDEmpresa=1 ");
            //res.first();
            //res.
            int cantColums = res.getMetaData().getColumnCount();
            int cantRows = 0;
            System.out.println(res.last());
            if(res.last())
            cantRows = res.getRow();
            System.out.println("CANT ROWS:"+cantRows);
            System.out.println(res.first());
            while(!res.isAfterLast()){
            System.out.print("Row:"+res.getRow()+"[");
            for(int i=0;i<cantColums;i++)
            System.out.print(res.getObject(i+1) +"-");
            System.out.println("]");
            res.next();
            }
            cnx.cerrarConexion();*/
            ArraySQL arra = cnx.consulta("SELECT * FROM CLIENTE WHERE Nombre = ? ",new Object[]{"Cesar"});
            arra.first();
            System.out.println(arra.previus());
            while (arra.masTuplas()) {
                System.out.println("Fil:" + arra.getFila() + "[" + arra.getData("idCliente") + "-" + arra.getData("idEmpresa") + "-" + arra.getData("IDClienteSi") + "-" + arra.getData("Nombre") + "]");
                arra.next();
            }
            arra.first();
            System.out.println(arra.previus());
            while (arra.masTuplas()) {
                System.out.print("Row:" + arra.getFila() + "[");
                for (int i = 0; i < arra.getCantColumns(); i++) {
                    System.out.print(arra.getData(i) + "-");
                }
                System.out.println("]");
                arra.next();
            }
            for (int i = 0; i < arra.getCantColumns(); i++) {
                System.out.print(arra.getColumName(i)+"***");
            }
            boolean bol = Boolean.valueOf("true");
            System.out.println(bol);
            /*Map contenedor = new HashMap();
            contenedor.put("col1", "2");
            contenedor.put("col10", "0");
            contenedor.put("col25", "1");
            System.out.println( contenedor.get("col25") );
            System.out.println( contenedor.get("col10") );*/

        } catch (SQLException ex) {
            Logger.getLogger(ConexionMSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ConexionMSQL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
