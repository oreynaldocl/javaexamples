/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package conexion;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ILACYERO
 */
public class ArraySQL implements Serializable{
    /**
     * Este elemento tiene los datos TANTO LAS COLUMNAS COMO SUS VALORES<br/>
     * La primera FILA tiene las NOMBRES_COLUMNA y las siguientes LOS VALORES
     */
    Object[][] datos;
    /***/
    Map colums;
    // public Has
    private int fila;
    public ArraySQL(Object[][] datos){
        this.datos = datos;
        fila = 1;
        prepararColumnas();
    }
    private void prepararColumnas(){
        colums=new HashMap();
        Object[] cols = datos[0];
        for(int i=0;i<cols.length;i++)
            colums.put(String.valueOf(cols[i]).toLowerCase(),String.valueOf(i));
    }
    /**
     * Indica la fila que actualmente se esta viendo
     */
    public int getFila(){
        return fila-1;
    }
    /**
     * Indica la fila que se quiere ver, este valor tiene que IR DE 1 A N-1, siendo N la cantidad de filas
     */
    public void setFila(int fila) throws Exception{
        if(fila<0 || fila>=getCantTuplas())
            throw new Exception("La fila tiene un valor incorrecto min:1 -- max:"+getCantTuplas());
        else
            this.fila=fila+1;
    }
    /**
     * Indica la cantidad de TUPLAS QUE SE OBTUVO
     */
    public int getCantTuplas(){
        return datos.length-1;
    }
    public int getCantColumns(){
        return datos[0].length;
    }
    public boolean masTuplas(){
        return fila<datos.length;
    }
    public boolean next(){
        if(fila<=getCantTuplas()){
            fila++;
            return true;
        }
        return false;
    }
    public boolean previus(){
        if(fila>1){
            fila--;
            return true;
        }
        return false;
    }
    public boolean first(){
        if(getCantTuplas()>0){
            fila = 1;
            return true;
        }
        return false;
    }
    public boolean last(){
        if(getCantTuplas()>0){
            fila = getCantTuplas();
            return true;
        }
        return false;
    }
    /**
     * Se manejara de 0 A N-1, siendo N la cantidad de COLUMNAS
     */
    public Object getData(int column) throws Exception{
        if(column>=0 && column<getCantColumns())
            return datos[fila][column];
        else
            throw new Exception("NO SE PUEDE LEER ELEMENTO, COLUMNA FUERA DE RANGO min:1 -- max:"+getCantColumns());
    }
    public Object getData(int row,int column) throws Exception{
        if(column>=0 && column<getCantColumns() && row>0 && row<getCantColumns())
            return datos[row][column];
        else
            throw new Exception("NO SE PUEDE LEER ELEMENTO, COLUMNA FUERA DE RANGO min:1 -- max:"+getCantColumns());
    }
        /**
     * Se manejara de 1 A N-1, siendo N la cantidad de COLUMNAS
     */
    public Object getData(String column) throws Exception{
        int posColum = getColumna(column);
        if(posColum>=0 && posColum<getCantColumns())
            return datos[fila][posColum];
        else
            throw new Exception("NO SE PUEDE LEER ELEMENTO, COLUMNA DESCONOCIDA "+column+", no existe en la tabla");
    }
    public Object getData(int row,String column) throws Exception{
        int posColum = getColumna(column);
        if(posColum>=0 && posColum<getCantColumns() && row>0 && row<getCantColumns())
            return datos[row][posColum];
        else
            throw new Exception("NO SE PUEDE LEER ELEMENTO, COLUMNA FUERA DE RANGO min:0 -- max:"+getCantColumns());
    }
    private int getColumna(String colum){
        Object obj= colums.get(colum.toLowerCase());
        if(obj != null)
            return Integer.parseInt(String.valueOf(obj));
        return -1;
    }

    public String getColumName(int column) throws Exception{
        if(column>=0 && column<getCantColumns())
            return String.valueOf(datos[0][column]);
        else
            throw new Exception("ERROR LA COLUMNA ESTA FUERA DE RANGO Min:0--Max:"+getCantColumns());
    }
}
